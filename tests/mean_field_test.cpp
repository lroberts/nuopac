#include <iostream> 
#include <math.h> 
#include <vector>

#include "Tensor.hpp"
#include "Polarization.hpp"
#include "Constants.hpp"

using namespace nuopac; 

int main() {
  double T = 5.0;
  double M2 = 939.0;
  double M4 = 939.0; 
  double U2 = 0.0;
  double U4 = 0.0;
  double M3 = 0.0;
  
  //double full = 1.0; 
  //double fullWM = 1.0; 
  //double elastic = 1.0; 
  
  const double densFac = pow(Constants::HBCFmMeV, 3); 
  
  // Test that the simple EoS stuff is working correctly  
  double n2 = 1.e-12*densFac;
  double n4 = 1.e-12*densFac;
  FluidState st = FluidState::StateFromDensities(T, M2, M4, n2, n4, U2, U4, M3, n2);
  FluidState stInverse(T, M2, M4, st.Mu2, st.Mu4, U2, U4, M3, st.Mu3);   
  if (fabs(1.0 - stInverse.n2/n2)>1.e-8) { 
    std::cerr << "Bad density find in n2: " << n2 
    << " " << stInverse.n2 << std::endl;
    return 1; 
  } 
  if (fabs(1.0 - stInverse.n4/n4)>1.e-8) {
    std::cerr << "Bad density find in n4: " << n4
    << " " << stInverse.n4 << std::endl; 
    return 1;
  }   
  
  // Now test a neutral current cross section in the low density, low energy limit 
  WeakCouplings nscat = WeakCouplings::NeutronScattering(); 
  nscat.F2 = 0.0; 
  Polarization pol(st, nscat, false);
  
  double E1 = 10.0; 
  double full = pol.CalculateInverseMFP(E1)/Constants::HBCFmMeV*1.e13; 
  
  double sig0 = 
      4.0*pow(Constants::GfMeV*E1*Constants::HBCFmMeV, 2)/Constants::Pi; 
  sig0 = sig0*(pow(nscat.Cv, 2) + 3.0*pow(nscat.Ca, 2))/4.0;
  double elastic = sig0*st.effectiveDensity/densFac*1.e13; 
  std::cout << "Neutral current test: " << full << " " << elastic << " " 
      << 1.0-full/elastic << std::endl;
  if (fabs((full-elastic)/full)>1.e-2) return 1;
  
  // Now test a charged current cross section 
  E1 = 1.0;
  double deltaM = 1.293; 
  bool antiNu = false; 
  M4 = M2 - deltaM; 
  M3 = 0.511;
  n2 = 1.e-8*densFac*0.9;
  n4 = 1.e-8*densFac*0.1;
  st = FluidState::StateFromDensities(T, M2, M4, n2, n4, U2, U4, M3); 
  
  WeakCouplings ncap = WeakCouplings::NuCapture(); 
  ncap.F2 = 0.0;
  pol = Polarization(st, ncap, antiNu, false); 
  double noWm = pol.CalculateInverseMFP(E1)/Constants::HBCFmMeV*1.e13; 
  
  ncap = WeakCouplings::NuCapture(); 
  pol = Polarization(st, ncap, antiNu); 
  double Wm = pol.CalculateInverseMFP(E1)/Constants::HBCFmMeV*1.e13; 
  
  // Elastic approximation to the cross section  
  sig0 = 4.0*pow(Constants::GfMeV*(E1+deltaM)*Constants::HBCFmMeV, 2)
      / Constants::Pi; 
  // We divide by 16.0 instead of 4.0 because of our convention for the coupling 
  // constants
  sig0 *= (1.0*pow(ncap.Cv, 2) + 3.0*pow(ncap.Ca, 2))/4.0;
  sig0 *= sqrt(1.0 - pow(0.511/(E1+deltaM),2));
  elastic = sig0*st.n2/densFac*1.e13;

  
  std::cout << noWm << " " << Wm << " " << (Wm/elastic-1.0)*M2/E1 << " " 
      << elastic << " " << elastic/noWm - 1.0 << std::endl; 
  
  if (fabs((noWm-elastic)/noWm)>1.e-2) return 1;
   
  return 0;
}

