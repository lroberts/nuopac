%module polarization
%{
#include "Couplings.hpp"
#include "Polarization.hpp"
#include "PolarizationNonRel.hpp"
#include "Constants.hpp"
#include "FluidState.hpp"
%}

%include "Couplings.hpp"
%include "Polarization.hpp"
%include "PolarizationNonRel.hpp"
%include "Constants.hpp"
%include "FluidState.hpp"
