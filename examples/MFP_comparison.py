import numpy as np 
from polarization import * 
import matplotlib.pyplot as pl
import math 

HBC = Constants.HBCFmMeV

def MakeTemperaturePlot(T, coupling, chargedCurrent = False, antiNeutrino = False): 
'''
  Make plot of neutrino mean free path as a function of density for a 
  range of neutrino energies at a fixed temperature. 
'''

  M2 = 939.565 
  M4 = 938.272
  M3 = 0.511
  
  coup = WeakCouplings_NuCapture() 
  coupNoT = WeakCouplings_NuCapture() 
  coupNoT.F2 = 0.0 
  
  myfig = pl.figure(figsize=(10,8))
  myfig.subplots_adjust(left=0.14)
  myfig.subplots_adjust(bottom=0.12)
  myfig.subplots_adjust(top=0.97)
  myfig.subplots_adjust(right=0.965)
  myfig.subplots_adjust(hspace=0.05)
  
  colormap = pl.get_cmap('plasma') 
  
  sig0 = 4.0*(Constants.GfMeV*Constants.HBCFmMeV*0.511)**2/Constants.Pi; 
  sig0 = sig0*(coup.Cv**2 + 3.0*coup.Ca**2)/4.0;
  
  states = []
  for logn in np.arange(-4.0, 0.25, 0.1):
    ntot = 10.0**logn*HBC**3 
    state = FluidState_BetaEquilibriumConsistentPotential(T, M2, M4, ntot, M3, coupling) 
    if antiNeutrino: 
      state = FluidState_ReverseState(state)
    print state.n2/HBC**3, state.n4/HBC**3, state.n4/(state.n2 + state.n4),
    print state.U2 - state.U4
    states.append(state) 
     
  for Enu in np.arange(math.log10(3.3), 2.1, 0.5):
    idx = Enu/2.0 
    clr = colormap(idx)
    Enu = 10.0**Enu
    if Enu>100:
      Enu = Enu - Enu%10  
    nscat = []
    sigov = []
    sigovNoT = []
    sigovReddy = []
    sigElastic = []
    sigovNR = []
    for state in states: 
      print "Density: ", '{:10.3e}'.format(state.n2/HBC**3), '{:10.3e}'.format(state.n4/ntot),
      pol = Polarization(state, coup, antiNeutrino, False, True)
      polNoT = Polarization(state, coupNoT, antiNeutrino, False, True)
      polReddy = Polarization(state, coupNoT, antiNeutrino, True, True)
      polNR = PolarizationNonRel(state, coup, antiNeutrino, False, True)
      
      sigov.append(pol.CalculateInverseMFP(Enu)/HBC)
      sigovNoT.append(polNoT.CalculateInverseMFP(Enu)/HBC)
      sigovReddy.append(polReddy.CalculateInverseMFP(Enu)/HBC)
      sigovNR.append(polNR.CalculateInverseMFP(Enu)/HBC)
      
      Ee = Enu + state.M2 - state.M4 + state.U2 - state.U4 
      pe = np.sqrt(Ee**2 - M3**2)*(1.0 - 1.0/(np.exp((Ee - state.Mu3)/T) + 1.0))
      sigElastic.append(sig0*state.effectiveDensity/HBC**3*pe*Ee/0.511**2)
      nscat.append((state.n2 + state.n4)/HBC**3) 
      print sigov[-1], sigovNoT[-1], sigElastic[-1]
    
    nscat = np.array(nscat) 
    sigov = np.array(sigov)
    sigovNoT = np.array(sigovNoT)
    sigElastic = np.array(sigElastic)
    norm = 1.e-15*np.ones(nscat.shape)  

    p0, = pl.semilogx(nscat/0.16, sigElastic/norm, linewidth=2.0, 
        color= clr, linestyle=':', dash_capstyle='round') 
    p1, = pl.semilogx(nscat/0.16, sigov/norm, 
        color = clr, linewidth=2.0, dash_capstyle='round') 
    p2, = pl.semilogx(nscat/0.16, sigovNoT/norm, 
        color = clr, linestyle='--', linewidth=2.0, dash_capstyle='round') 
    p3, = pl.semilogx(nscat/0.16, sigovReddy/norm, 
        color= clr, linestyle='-.', linewidth=2.0, dash_capstyle='round') 
    p4, = pl.semilogx(nscat/0.16, sigovNR/norm, 
        color= clr, linestyle=(0,(6.0,4.0,2.0,4.0,2.0,4.0)), linewidth=2.0, dash_capstyle='round') 
    print  '{:3.0f}'.format(Enu)
    pl.text(2.e-3, sigov[0]/norm[0]*0.75, '{:3.0f}'.format(Enu), fontsize=24, color=clr)  
    
  pl.annotate('$T = ' + '{:2.0f}'.format(T) + '\\, {\\rm MeV}$', xy=(0.6, 0.05), 
      xycoords = 'axes fraction', fontsize=28)
  
  pl.xscale('log') 
  pl.yscale('log') 
  pl.xlim([1.e-3, 4.0]) 
  pl.ylim([1.e-6, 1.e3])
  
  # modify legend
  mylegend = pl.legend( (p1,p2,p3,p0,p4), 
      ("Full", "$F_2 = 0.0$", "RPL98", "$\\vec{q}=0$", "Constant $\\Lambda^{\\mu\\nu}$"), 
      loc='upper left', ncol=2)
  mylegend.draw_frame(False)
  mylegendtext = mylegend.get_texts()
  mylegendlines = mylegend.get_lines()
  pl.setp(mylegendtext, fontsize=28)
  pl.setp(mylegendlines, linewidth=2.5, color='black') 
  
  pl.xlabel('$n_{\\rm tot}/n_s$')
  pl.ylabel('$1/\\lambda \\, [{\\rm m}^{-1}]$')

  title = "MFP_T" + '{:02.0f}'.format(T) 
  if (abs(coupling)<1.e-10):
     title += "_Free"   
  else: 
     title += "_Interacting"
  
  if not antiNeutrino:
    title += "_nue_n_e_p.pdf" 
  else:
    title += "_nueb_p_pos_n.pdf" 
        
  print title
  pl.savefig(title, bbox_inches='tight')
  pl.close()



antiNu = False
for T in [10]: #, 15, 30, 60]:
  MakeTemperaturePlot(T, 0.0, antiNeutrino = antiNu)
  coupling = (8.5416/(HBC*3.8970))**2
  coupling = 40.0/(0.16*HBC**3) # 40 MeV at saturation
  MakeTemperaturePlot(T, coupling, antiNeutrino=antiNu)

