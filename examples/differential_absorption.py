# The python polarization libraries must be installed somewhere 
# your python distribution will find them   
from polarization import * 

# Set the properties of the background medium 
T = 5.0
M2 = 939.0
M4 = 939.0 
Mu2 = M2 + 2.0
Mu4 = M4 - 2.0
Mu3 = 0.0
U2 = 0.0
U4 = 0.0
M3 = 0.0

state = FluidState(T, M2, M4, Mu2, Mu4, U2, U4, M3, Mu3)

# Set the couplings for the interaction 
ncap = WeakCouplings_NuCapture()

# Build an instance of the polarization class with the above fluid state and 
# couplings
antiNeutrino = False 
pol = Polarization(state, ncap, antiNeutrino)

# Calculate the inverse MFP (in units of cm) for a 12 MeV neutrinos 
E1 = 12.0
q0 = -1.0
mu13 = 0.9  

# Calculate various pieces of the differential csection 
dsigdq0dmuoV = pol.CalculateDGamDq0Dmu13(E1, q0, mu13)
dsigdq0oV = pol.CalculateDGamDq0(E1, q0);  
ilam = pol.CalculateInverseMFP(E1)/Constants.HBCFmMeV*1.e13; 

print "Enu =  ", E1, " [MeV]"
print "1/lam =  ", ilam, " [cm^-1]"

