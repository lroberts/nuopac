#include <iostream> 
#include <math.h> 
#include <vector>

#include "Tensor.hpp"
#include "Polarization.hpp"
#include "Constants.hpp"

using namespace nuopac;

int main() {
  // Set the properties of the background medium 
  double T = 5.0;
  double M2 = 939.0;
  double M4 = 939.0; 
  double Mu2 = M2 + 2.0;
  double Mu4 = M4 - 2.0;
  double Mu3 = 0.0;
  double U2 = 0.0;
  double U4 = 0.0;
  double M3 = 0.0;
  
  FluidState state(T, M2, M4, Mu2, Mu4, U2, U4, M3, Mu3);   
  
  // Set the couplings for the interaction 
  WeakCouplings ncap = WeakCouplings::NuCapture(); 

  // Build an instance of the polarization class with the above fluid state and 
  // couplings
  bool antiNeutrino = false; 
  Polarization pol(state, ncap, antiNeutrino);

  // Calculate the inverse MFP (in units of cm) for a 12 MeV neutrinos 
  double E1 = 12.0; 
  double q0 = -1.0;
  double mu13 = 0.9;  
  
  // Calculate various pieces of the differential csection 
  double dsigdq0dmuoV = pol.CalculateDGamDq0Dmu13(E1, q0, mu13);  
  double dsigdq0oV = pol.CalculateDGamDq0(E1, q0);  
  double ilam = pol.CalculateInverseMFP(E1)/Constants::HBCFmMeV*1.e13; 
  
  std::cout << "Enu =  " << E1 << " [MeV]" <<std::endl; 
  std::cout << "1/lam =  " << ilam <<  " [cm^-1]" << std::endl; 
  
  return 0; 
}

