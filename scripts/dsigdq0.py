import numpy as np 
from polarization import * 
import matplotlib.pyplot as pl
from plot_defaults import *

HBC = Constants.HBCFmMeV

def plot_dsigdq0(plt, Enu, q0arr, state, couplings, color, linestyle='-', antiNeutrino=False, doReddy=False):
  pol = Polarization(state, couplings, antiNeutrino, doReddy) 
  dsigdq0 = []
  for q0 in q0arr: 
    dsigdq0.append(pol.CalculateDSigDq0(Enu, q0))
     
  p1, = plt.plot(q0arr, np.array(dsigdq0), color = color, linewidth=1.5, linestyle=linestyle) 
  return np.nanmax(dsigdq0) 

def write_conditions(plt, st):    
  Tline = '$T = ' + '{:2.0f}'.format(st.T) + '\\, {\\rm MeV}$'
  Nline = '$n_{2,4} = \\{' + '{:4.3f},{:4.3f}'.format(st.n2/HBC**3, st.n4/HBC**3) + '\\}$ fm$^{-3}$'
  Mline = '$M_{2,3,4} = \\{' + '{:4.1f},{:4.3f},{:4.1f}'.format(st.M2, st.M3, st.M4) + '\\}$ MeV' 
  Uline = '$\Delta U =' + '{:2.0f}'.format(st.U2 - st.U4) + '$ MeV' 
  plt.annotate(Tline, xy=(0.02,0.95), xycoords = 'axes fraction', fontsize=20)
  plt.annotate(Nline, xy=(0.02,0.88), xycoords = 'axes fraction', fontsize=20)
  plt.annotate(Mline, xy=(0.02,0.81), xycoords = 'axes fraction', fontsize=20)
  plt.annotate(Uline, xy=(0.02,0.74), xycoords = 'axes fraction', fontsize=20)


antiNeutrino = False
T = 5.0
M2 = 939.565
M4 = 938.272
M3 = 0.0
U2 = 3.0
U4 = 0.0
 
n2 = 1.e-1*HBC**3
n4 = 3.e-2*HBC**3

state = FluidState_StateFromDensities(T, M2, M4, n2, n4, U2, U4, M3)
stateReddy = FluidState_StateFromDensities(T, M2, M4, n2, n4, U2, U4, 0.0)

coup = WeakCouplings_NuCapture() 
coupNoWM = WeakCouplings_NuCapture() 
coupNoWM.F2 = 0.0

myfig = pl.figure(figsize=(10,8))
myfig.subplots_adjust(left=0.14)
myfig.subplots_adjust(bottom=0.12)
myfig.subplots_adjust(top=0.9)
myfig.subplots_adjust(right=0.95)
myfig.subplots_adjust(hspace=0.05)

colormap = pl.get_cmap('plasma') 

mx = 0.0 
enumax =  2.0
enumin = -1.0
delta = 0.2
mx = 0.0
for Enu in np.arange(enumin, enumax+delta, delta):
  idx = (Enu - enumin)/(enumax-enumin)
  clr = colormap(idx)
  Enu = 10.0**Enu
  q0arr = np.arange(-15.0, Enu - M3, 0.1)
  mx = max([plot_dsigdq0(pl, Enu, q0arr, state, coupNoWM, clr, '-', antiNeutrino=antiNeutrino), mx])
  mx2 = max([plot_dsigdq0(pl, Enu, q0arr, stateReddy, coupNoWM, clr, '--', antiNeutrino=antiNeutrino, doReddy=True), mx])

write_conditions(plt, state) 

pl.yscale('log') 
pl.xlim([q0arr[0], 10.0]) 
pl.ylim([1.e-2*mx, mx*1.1])

pl.ylabel('$d \\sigma / d q_0$')
pl.xlabel('$q_0 \\, {\\rm [MeV]}$')

# Put in a fancy colorbar 
axc = myfig.add_axes([0.21, 0.97, 0.7, 0.03])
norm = mpl.colors.Normalize(vmin=enumin, vmax=enumax) 
cbar = mpl.colorbar.ColorbarBase(axc, cmap=colormap, norm=norm, 
    orientation='horizontal', )
cbar.solids.set_edgecolor("face")
cbar.set_label("$Log_{10}(\\epsilon_\\nu) \, {\\rm [MeV]}$", x = -0.15, y = 0.5, labelpad=-50, fontsize=18)


pl.savefig("Differential.pdf", bbox_inches='tight')

