import numpy as np 
from polarization import * 
import matplotlib.pyplot as pl
from plot_defaults import *

HBC = Constants.HBCFmMeV

def MakeTemperaturePlot(T, coupling, chargedCurrent = False): 
  M2 = 939.565 
  M4 = 938.272
  M3 = 0.511
  
  antiNeutrino = False
  coup = WeakCouplings_NuCapture() 
  coupNoT = WeakCouplings_NuCapture() 
  coupNoT.F2 = 0.0 
  
  myfig = pl.figure(figsize=(10,8))
  myfig.subplots_adjust(left=0.14)
  myfig.subplots_adjust(bottom=0.12)
  myfig.subplots_adjust(top=0.97)
  myfig.subplots_adjust(right=0.965)
  myfig.subplots_adjust(hspace=0.05)
  
  colormap = pl.get_cmap('plasma') 
  
  sig0 = 4.0*(Constants.GfMeV*Constants.HBCFmMeV*0.511)**2/Constants.Pi; 
  sig0 = sig0*(coup.Cv**2 + 3.0*coup.Ca**2)/16.0;
  
  states = []
  for logn in np.arange(-4.0, 0.25, 0.1):
    ntot = 10.0**logn*HBC**3 
    state = FluidState_BetaEquilibriumConsistentPotential(T, M2, M4, ntot, M3, coupling) 
    print state.n2/HBC**3, state.n4/HBC**3, state.n4/(state.n2 + state.n4),
    print state.U2 - state.U4
    states.append(state) 
     
  for Enu in np.arange(0.0, 2.1, 0.5):
    idx = Enu/2.0 
    clr = colormap(idx)
    Enu = 10.0**Enu 
    nscat = []
    sigov = []
    sigovNoT = []
    sigovReddy = []
    sigElastic = []
    for state in states: 
      print "Density: ", '{:10.3e}'.format(state.n2/HBC**3), '{:10.3e}'.format(state.n4/ntot),
      pol = Polarization(state, coup, antiNeutrino, False, True)
      polNoT = Polarization(state, coupNoT, antiNeutrino, False, True)
      polReddy = Polarization(state, coupNoT, antiNeutrino, True, True)
      
      sigov.append(pol.CalculateTotalCsec(Enu)/HBC)
      sigovNoT.append(polNoT.CalculateTotalCsec(Enu)/HBC)
      sigovReddy.append(polReddy.CalculateTotalCsec(Enu)/HBC)
      
      Ee = Enu + state.M2 - state.M4 + state.U2 - state.U4 
      pe = np.sqrt(Ee**2 - M3**2)*(1.0 - 1.0/(np.exp((Ee - state.Mu3)/T) + 1.0))
      sigElastic.append(sig0*state.effectiveDensity/HBC**3*pe*Ee/0.511**2)
      nscat.append(state.n2/HBC**3) 
      print sigov[-1], sigovNoT[-1], sigElastic[-1]
    
    nscat = np.array(nscat) 
    sigov = np.array(sigov)
    sigovNoT = np.array(sigovNoT)
    sigElastic = np.array(sigElastic)
    
    p0, = pl.semilogx(nscat, sigElastic/nscat, linewidth=1.0, 
        color= clr, linestyle=':', dash_capstyle='round') 
    p1, = pl.semilogx(nscat, sigov/nscat, 
        color = clr, linewidth=2.0, dash_capstyle='round') 
    p2, = pl.semilogx(nscat, sigovNoT/nscat, 
        color = clr, linestyle='--', linewidth=2.0, dash_capstyle='round') 
    p3, = pl.semilogx(nscat, sigovReddy/nscat, 
        color= clr, linestyle='-.', linewidth=1.5, dash_capstyle='round') 
    #pl.fill_between(nscat, sigov/nscat, sigovNoT/nscat, alpha = 0.2, facecolor=clr) 
  
  pl.annotate('$T = ' + '{:2.0f}'.format(T) + '\\, {\\rm MeV}$', xy=(0.7, 0.05), 
      xycoords = 'axes fraction', fontsize=28)
  
  pl.xscale('log') 
  pl.yscale('log') 
  pl.xlim([1.e-4, 1.0]) 
  #pl.ylim([0.7, 60.0])
  
  # modify legend
  mylegend = pl.legend( (p1,p2,p3,p0), 
      ("Full", "$F_2 = 0.0$", "Reddy '98", "Elastic"), 
      loc='upper left', ncol=2)
  mylegend.draw_frame(False)
  mylegendtext = mylegend.get_texts()
  mylegendlines = mylegend.get_lines()
  pl.setp(mylegendtext, fontsize=28)
  pl.setp(mylegendlines, linewidth=2.5, color='black') 
  
  pl.xlabel('$n_2 \\, [{\\rm fm}^{-3}]$')
  pl.ylabel('$\\sigma \\, [{\\rm fm}^2]$')

  title = "MFP_T" + '{:02.0f}'.format(T) 
  if (abs(coupling)<1.e-10):
     title += "_Free"   
  else: 
     title += "_Interacting"
  
  if M2 > M4:
    title += "_nue_n_e_p.pdf" 
  else:
    title += "_nueb_p_pos_n.pdf" 
        
  print title
  pl.savefig(title, bbox_inches='tight')
  pl.close()


for T in [10, 15, 30, 60]:
  MakeTemperaturePlot(T, 0.0)
  coupling = (8.5416/(HBC*3.8970))**2
  MakeTemperaturePlot(T, coupling)

