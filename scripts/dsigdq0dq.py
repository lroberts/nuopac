import numpy as np 
from polarization import * 
import matplotlib.pyplot as pl
from plot_defaults import *

HBC = Constants.HBCFmMeV

def MakeDifferentialCsecPlot(Enu, state, coup, antiNeutrino, outName):
  myfig = pl.figure(figsize=(10,8))
  myfig.subplots_adjust(left=0.14)
  myfig.subplots_adjust(bottom=0.12)
  myfig.subplots_adjust(top=0.97)
  myfig.subplots_adjust(right=0.965)
  myfig.subplots_adjust(hspace=0.05)

  colormap = pl.get_cmap('viridis') 

  pol = Polarization(state, coup, antiNeutrino, False, False)
  polBlock = Polarization(state, coup, antiNeutrino, False, True)
  
  muArr = np.arange(-1.0, 1.001, 0.01) 
  q0max = -(state.U2 - state.U4 + state.M2 - state.M4) 
  q0min = min([-2 * Enu, 2 * q0max])
  q0max = (Enu - M3)*0.99
  q0Arr = np.arange(q0min, q0max, (q0max - q0min)/500) 
  dsigdq0dq = np.zeros((len(q0Arr), len(muArr))) 
  dsigdq0dqBlock = np.zeros((len(q0Arr), len(muArr))) 

  for (i, q0) in enumerate(q0Arr): 
    for (j, mu) in enumerate(muArr):
      q = pol.GetqFromMu13(Enu, q0, mu)
      dsigdq0dq[i,j] = max([pol.CalculateDifCsec(Enu, q0, q), 1.e-100])
      dsigdq0dqBlock[i,j] = max([polBlock.CalculateDifCsec(Enu, q0, q), 1.e-100])

  levels = np.array([1.e-3, 1.e-2, 1.e-1, 0.2, 0.4, 0.6, 0.8, 1.0])

  mx = np.nanmax(dsigdq0dq) 
  dsigdq0dq = np.log10(dsigdq0dq/mx)
  
  mx = np.nanmax(dsigdq0dqBlock) 
  dsigdq0dqBlock = np.log10(dsigdq0dqBlock/mx)
  
  levels = np.log10(levels)
  plt.contourf(q0Arr, muArr, np.transpose(dsigdq0dq), levels, cmap=colormap)
  CS4 = plt.contour(q0Arr, muArr, np.transpose(dsigdq0dqBlock), levels, 
      colors='k', linewidths=1.5, linestyles='-', capstyle='round') 
  
  fmt = {} 
  for level, lin in zip(CS4.levels, levels):
    l = (10.0**lin)
    fmt[level] = supermongolikeN4a(l, 0.0)
    fmt[level] = str(l)
  
  plt.clabel(CS4, CS4.levels[1:], fmt=fmt, colors='w', fontsize=14)
  
  xpos = 0.72
  pl.annotate('$E_1 = ' + '{:2.0f}'.format(Enu) + '\\, {\\rm MeV}$', xy=(xpos,0.95), 
      xycoords = 'axes fraction', fontsize=20)
  pl.annotate('$T = ' + '{:2.0f}'.format(state.T) + '\\, {\\rm MeV}$', xy=(xpos,0.9), 
      xycoords = 'axes fraction', fontsize=20)
  pl.annotate('$n_{\\rm tot}  = ' + '{:4.3f}'.format((state.n2 + state.n4)/HBC**3) 
      + '\\, {\\rm fm}^{-3}$', xy=(xpos,0.85), xycoords = 'axes fraction', fontsize=20)
  pl.annotate('$Y_4  = ' + '{:3.2f}'.format(state.n4/(state.n2 + state.n4)) 
      + '$', xy=(xpos,0.8), xycoords = 'axes fraction', fontsize=20)
  pl.annotate('$\\Delta U  = ' + '{:3.2f}'.format(state.U2 - state.U4) 
      + '\\, {\\rm MeV}$', xy=(xpos,0.75), xycoords = 'axes fraction', fontsize=20)
  
  pl.xlim([q0min, q0max]) 
  pl.ylim([-1, 1])

  pl.ylabel('$\\mu$')
  pl.xlabel('$q_0 \\, {\\rm [MeV]}$')

  pl.savefig(outName, bbox_inches='tight')
  pl.close()

T = 5
M2 = 939.565 
M4 = 938.272
M3 = 0.511
Enu = 12.0 
coupling = (8.5416/(HBC*3.8970))**2

antiNeutrino = False
coup = WeakCouplings_NuCapture()

logntots = np.arange(-4, 0.0, 0.2) 
for i, ntot in enumerate(logntots): 
  ntot = 10.0**ntot*HBC**3
  fname = "./DifferentialPlots/Differential_Enu10_T5_{:04d}.pdf".format(i) 
  print fname, ntot/HBC**3, 
  try: 
    state = FluidState_BetaEquilibriumConsistentPotential(T, M2, M4, ntot, M3, 
        coupling) 
    print state.n4/(state.n2 + state.n4) 
    MakeDifferentialCsecPlot(Enu, state, coup, False, fname)
    print ' Done.'
  except:
    print ' Failed!'



