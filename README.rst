Neutrino Opacity Code README
============================
This repository provides a library for calculating neutral and charged 
current neutrino interaction rates in the mean field approximation as 
described in `ArXiv:1612.02764 <https://arxiv.org/abs/1612.02764>`_. 
Python bindings to the code are available if SWIG is installed. We 
provide a number of example usages of the code in the examples 
subdirectory. 

THE LIBARY ASSUMES ALL UNITS ARE IN MEV.

Requirements
============
* cmake 2.8.8+ 
* Compiler supporting C++11 standard. 

Optional Packages
=================
* To make Python bindings for the code SWIG must be installed, as well as Python 2.7. 
* GSL integration routines, without GSL the tests will fail but the underlying 
  library works fine. 

Installation 
============

1. Create a directory in which to build, for instance ./build 

2. Set up makefiles and build structure using cmake 

::
 
$ cd <build_dir>
$ cmake  -DCMAKE_INSTALL_PREFIX=<install_dir> <base_dir> 
where <base_dir> is the base directory of the opacity code files (i.e. the
directory in which this install guide is contained) and <install_dir> is the
directory in which the libraries will be installed when running make install.

3. Compile and test code:
  
:: 

$ make 
$ make test 

This will build the code and run a series of tests to make sure everything is
working properly.

4. Install the libraries:

::

$ make install

Code Documentation
==================

Documentation of the class structure and available methods can be produced using 
`Doxygen <http://www.doxygen.org/>`_. A doxygen script is contained in the base 
directory of the repository. To produce this documentation, run:

::

$ cd <base_dir>
$ mkdir doc 
$ doxygen doxygen_script


Random Notes
============

If you change call signatures in functions that are wrapped by SWIG, you will 
often need to make clean; make install to get these changes to propagate to python.

Developing
==========

We plan to continue development of this code. If you would like to contribute to 
the development of the code or would want to make changes to the code that may be
incorporated into the main version of the code later, we suggest forking this 
repository on bitbucket and sending updates to the main repository via pull 
requests. Please contact robertsl@nscl.msu.edu for any questions.

License
======= 
Copyright (c) 2016 Luke F. Roberts.

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
